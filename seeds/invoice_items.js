
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('invoice_items').del()
    .then(function () {
      // Inserts seed entries
      return knex('invoice_items').insert([
        {
          invoice_id:"1",
          product_name: "Gold Ear Rings",
          product_price: "4570",
          product_weight: "8",
          quantity:1,
          gst:"4",
          total:38022.40
        },
        {
          invoice_id:"2",
          product_name: "Gold Ring",
          product_price: "4270",
          product_weight: "4.5",
          quantity:2,
          gst:"5",
          total:40351.5
        },
        {
          invoice_id:"1",
          product_name: "Gold Ring",
          product_price: "4270",
          product_weight: "4.5",
          quantity:3,
          gst:"5",
          total:60527.25
        }
      ]);
    });
};

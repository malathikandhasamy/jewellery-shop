
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('prices').del()
    .then(function () {
      // Inserts seed entries
      return knex('prices').insert([
        {
          price_name:"24k Gold",
          price:"4570",
          gst:"4"
        },
        {
          price_name:"22k Gold",
          price:"4270",
          gst:"5"
        },
        {
          price_name:"18k Gold",
          price:"3570",
          gst:"2"
        },
        {
          price_name:"Silver",
          price:"55",
          gst:"1"
        }
      ]);
    });
};

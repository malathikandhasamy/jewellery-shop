
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          "name": "Cochran Finch",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": false
        },
        {
          "name": "Hopper Schroeder",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": false
        },
        {
          "name": "Pansy Velez",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": false
        },
        {
          "name": "Roth Warner",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": false
        },
        {
          "name": "Wolf Oneill",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": true
        },
        {
          "name": "Petty Cash",
          "password": "ff7bd97b1a7789ddd2775122fd6817f3173672da9f802ceec57f284325bf589f",
          "is_active": true
        }
      ]);
    });
};

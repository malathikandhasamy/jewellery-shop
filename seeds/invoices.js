
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('invoices').del()
    .then(function () {
      // Inserts seed entries
      return knex('invoices').insert([
       {
         created_by:"2",
         to_address:"39/1,Gopal Street",
         total:"98549.65"
       },
       {
        created_by:"3",
        to_address:"50/1,Kandhasamy  Street",
        total:"40351.50"
      }
      ]);
    });
};

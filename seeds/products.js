
exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(function () {
      // Inserts seed entries
      return knex('products').insert([
        {
          product_name: "Gold Ring",
          product_price: "2",
          product_weight: "4.5",
        },
        {
          product_name: "Gold Ear Rings",
          product_price: "1",
          product_weight: "8",
        },
        {
          product_name: "Silver Chain",
          product_price: "4",
          product_weight: "50",
        }
      ]);
    });
};

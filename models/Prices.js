var db = require('../utils/db');
var {Model} = require('objection')

Model.knex(db)
class Prices extends Model{
    static get tableName(){
        return 'prices'
    }
}

module.exports = Prices;
var db = require('../utils/db');
var {Model} = require('objection')

Model.knex(db)
class Users extends Model{
    static get tableName(){
        return 'users'
    }
}

module.exports = Users;
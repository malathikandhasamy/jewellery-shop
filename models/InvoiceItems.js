var db = require('../utils/db');
var {Model} = require('objection')

Model.knex(db)
class InvoiceItems extends Model{
    static get tableName(){
        return 'invoice_items'
    }
}

module.exports = InvoiceItems;
var db = require('../utils/db');
var {Model} = require('objection');
const InvoiceItems = require('./InvoiceItems');

Model.knex(db)
class Invoices extends Model{
    static get tableName(){
        return 'invoices'
    }
    static relationMappings = {
        items: {
          relation: Model.HasManyRelation,
          modelClass: InvoiceItems,
          filter:query=>query.select(),
          join: {
            from: 'invoices.id',
            to: 'invoice_items.invoice_id'
          }
        }
      };
}

module.exports = Invoices;
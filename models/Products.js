var db = require('../utils/db');
var { Model } = require('objection');
const Prices = require('./Prices');
const { query } = require('./Prices');

Model.knex(db)
class Products extends Model {
    static get tableName() {
        return 'products'
    }
    static get idColumn() {
        return 'product_code';
    }
    static relationMappings = {
        price_details: {
          relation: Model.HasOneRelation,
          modelClass: Prices,
          filter:query=>query.select('id','price_name','price','gst'),
          join: {
            from: 'products.product_price',
            to: 'prices.id'
          }
        }
      };

}

module.exports = Products;
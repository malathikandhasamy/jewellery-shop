const express = require('express')
const { decryption } = require('./utils/cypto');
const cors = require('cors')
require('dotenv').config()

const app = express()
const port = process.env.PORT || 3000
const usersRouter = require('./routes/users')
const productsRouter = require('./routes/products')
const invoiceRouter = require('./routes/invoices')


app.use(cors())
app.use(express.urlencoded({
    extended: true
}));
app.use(express.text());
app.use(decryption)

app.use('/users', usersRouter)
app.use('/products', productsRouter)
app.use('/invoices', invoiceRouter)



app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
})
const express = require('express');
const Invoices = require('../models/Invoices');
var SHA256 = require("crypto-js/sha256");

const router = express.Router();

const bindRes = require('../utils/bindRes');
const validateRequest = require('../utils/validate');
const { verifyJWToken, createJWToken } = require('../utils/jwt');
const Products = require('../models/Products');
const InvoiceItems = require('../models/InvoiceItems');
const moment = require('moment');

var createInvoicesRule = {

}

router.get('/', verifyJWToken, async (req, res) => {
    try {
        let { from, to } = req.query
        let invoiceQuery = Invoices.query().select().withGraphFetched("items");
        if (from && to) {
            invoiceQuery.andWhere('created_at', '>', moment(from).format('YYYY-MM-D H:mm:ss')).andWhere('created_at', '<', moment(to).format('YYYY-MM-D H:mm:ss'))
        }
        result = await invoiceQuery
        bindRes(null, 'List Invoices', res, { result })
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Invoices.query().findOne({ id }).withGraphFetched('items');
        bindRes(null, 'List Select Invoices detail', res, { result })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

//create
router.post('/', verifyJWToken, validateRequest(createInvoicesRule), async (req, res) => {
    try {
        let { to_address, products } = req.body
        let { user_id } = req
        let result = await Invoices.query().insert({
            created_by: user_id,
            to_address,
            total: 0
        })
        let productsResult = await Products.query().whereIn('product_code', products.map(item => item.product_code)).withGraphFetched('price_details')
        var totalInvoice = 0;

        for (const product of productsResult) {
            var product_price = product.price_details.price;
            var product_weight = product.product_weight;
            // var quantity = products[inx].quantity;
            let { quantity } = products.find(f => f.product_code == product.product_code) || {}
            var gst = product.price_details.gst;
            var gstPrice = (product_price * product_weight * quantity) * (gst / 100)

            await InvoiceItems.query().insert({
                invoice_id: result.id,
                product_name: product.product_name,
                product_price: product_price,
                product_weight: product_weight,
                quantity: quantity,
                gst: gst,
                total: (product_price * product_weight * quantity) + gstPrice

            })
            totalInvoice += (product_price * product_weight * quantity) + gstPrice
        }

        await Invoices.query().update({
            total: totalInvoice
        })
        let invoiceDetail = await Invoices.query().findById(result.id).withGraphFetched('items')
        bindRes(null, 'Invoices added successfully', res, invoiceDetail)
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }

})
// update
router.put('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let { product_name, product_price, product_weight } = req.body
        let result = await Invoices.query().update({
            product_name,
            product_price,
            product_weight
        }).where({ product_code: id })
        if (result) {
            bindRes(null, 'Invoices updated successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }
})
//delete
router.delete('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Invoices.query().deleteById(id)
        if (result) {
            bindRes(null, 'Invoices deleted successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }

})




module.exports = router


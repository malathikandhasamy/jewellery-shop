const express = require('express');
const Users = require('../models/Users');
var SHA256 = require("crypto-js/sha256");

const router = express.Router();

const bindRes = require('../utils/bindRes');
const validateRequest = require('../utils/validate');
const { verifyJWToken, createJWToken } = require('../utils/jwt');
const { decryption } = require('../utils/cypto');

var createUserRule = {
    name: 'required',
    password: 'required',
    is_active: 'required'
}
var loginRule = {
    user_id: 'required',
    password: 'required'
}
router.get('/',verifyJWToken, async (req, res) => {
    try {
        let result = await Users.query().select();
        bindRes(null, 'List User', res, { result })
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/:id',verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        // let result = await Users.query().select().where({id}).first();
        // let result = await Users.query().findById(id);
        let result = await Users.query().findOne({ id });
        bindRes(null, 'List Select User detail', res, { result })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

//create
router.post('/', verifyJWToken,validateRequest(createUserRule), async (req, res) => {
    try {
        let { name, password, is_active } = req.body

        let result = await Users.query().insert({
            name: name,
            password: SHA256(password).toString(),
            is_active: is_active
        })
        bindRes(null, 'User added successfully', res, { result })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }

})
// update
router.put('/:id',verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let { name, password, is_active } = req.body
        let result = await Users.query().update({
            name: name,
            password: password,
            is_active: is_active
        }).where({ id })
        if (result) {
            bindRes(null, 'User updated successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }
})
//delete
router.delete('/:id',verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Users.query().deleteById(id)
        if (result) {
            bindRes(null, 'User deleted successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }

})

router.post('/login' ,validateRequest(loginRule), async (req, res) => {
    try {
        let { user_id, password } = req.body;
        let result = await Users.query().select().findOne({ id: user_id });
        if (result && SHA256(password).toString() === result.password) {
            let token = createJWToken({user_id:result.id})
            bindRes(null, 'Login Successfully', res, { ...result, token: token })
        }
        else {
            bindRes(true, 'Invalid user_id or Password', res)
        }
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})





module.exports = router


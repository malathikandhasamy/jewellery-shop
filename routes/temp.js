const express = require('express');
const Products = require('../models/Products');
var SHA256 = require("crypto-js/sha256");

const router = express.Router();

const bindRes = require('../utils/bindRes');
const validateRequest = require('../utils/validate');
const { verifyJWToken, createJWToken } = require('../utils/jwt');

var createProductRule = {
    name: 'required',
    password: 'required',
    is_active: 'required'
}

router.get('/', verifyJWToken, async (req, res) => {
    try {
        let result = await Products.query().select();
        bindRes(null, 'List Product', res, { result })
    }
    catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

router.get('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Products.query().findOne({ id });
        bindRes(null, 'List Select Product detail', res, { result })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }
})

//create
router.post('/', verifyJWToken, validateRequest(createProductRule), async (req, res) => {
    try {
        let { product_name, product_price, product_weight } = req.body

        let result = await Products.query().insert({
            product_name, 
            product_price,
            product_weight
        })
        bindRes(null, 'Product added successfully', res, { result })
    } catch (err) {
        bindRes(true, 'Request failed', res, err.toString())
    }

})
// update
router.put('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let { product_name, product_price, product_weight } = req.body
        let result = await Products.query().update({
            product_name,
            product_price,
            product_weight
        }).where({ id })
        if (result) {
            bindRes(null, 'Product updated successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }
})
//delete
router.delete('/:id', verifyJWToken, async (req, res) => {
    try {
        let { id } = req.params
        let result = await Products.query().deleteById(id)
        if (result) {
            bindRes(null, 'Product deleted successfully', res, { result })
        }
        else {
            bindRes(true, 'Request failed', res)
        }
    } catch (err) {
        bindRes(true, 'Request failed', res)
    }

})




module.exports = router


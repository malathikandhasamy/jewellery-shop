// Update with your config settings.
require('dotenv').config()

module.exports = {
  client: 'sqlite3',
  connection: {
    filename: 'jshop.sqlite'
  },

  migrations: {
    tableName: 'knex_migrations'
  }
};

// module.exports = {
//   client: 'mysql',
//   connection: {
//     database: process.env.DATABASE || "database",
//     user: process.env.USER_NAME || "user",
//     password: process.env.PASSWORD || "password"
//   },

//   migrations: {
//     tableName: 'knex_migrations'
//   }
// };

# Jewellery Shop

## API Endpoint
[malathi-js.herokuapp.com](https://malathi-js.herokuapp.com)


## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br>
[http://localhost:5000](http://localhost:5000).


### `npm run start`

Runs the app in the Production mode.<br>
[http://localhost:5000](http://localhost:5000).

### `npm run migrate`
Creating tables on database<br/>
[http://knexjs.org/#Migrations-API](http://knexjs.org/#Migrations-APIk)

### `npm run seed`
Insets default records on database<br/>
[http://knexjs.org/#Seeds-API](http://knexjs.org/#Seeds-API)

## Database Connectivity
[MySQL](https://www.npmjs.com/package/mysql), [Knex JS](http://knexjs.org), and [Objection JS](https://vincit.github.io/objection.js) packages are used to connect database and data manipulation

## Bearer Authentication
[jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) used for authentication <br />
```Authorization: Bearer <token>```

## Encryption and Decryption
Advanced Encryption Standard (AES) using [crypto-js](https://www.npmjs.com/package/crypto-js)

Online [https://runkit.com/malathikandhasamy/encryption](https://runkit.com/malathikandhasamy/encryption)

KEY: ```n2r4u7x!A%D*G-Ka```

## Request body validation
[validatorjs](https://www.npmjs.com/package/validatorjs) used to validate the request body


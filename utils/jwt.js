let jwt = require('jsonwebtoken');
let bindRes = require('./bindRes');

let SHA256 = '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'

let createJWToken = (data = "") => {
    let iat = new Date().getTime();
    let hours = 24 * 360;
    let exp = iat + (1000 * 60 * 60) * hours;
    return jwt.sign({ ...data, iat, exp }, SHA256)
}

let verifyJWToken = (req, res, next) => {

    let token = req.headers.authorization
    if (token) {
        token = token.split(" ")[1]
        jwt.verify(token, SHA256, (err, decode) => {
            if (err) return bindRes(err, 'Unexpected token', res);

            if ('exp' in decode && decode.exp - new Date().getTime() > 0) {
                let { user_id } = decode;
                req.user_id = user_id;
                next()
            }
            else {
                bindRes(true, 'Token expired', res)
            }
        })
    }
    else {
        bindRes(true, 'Token missing', res)
    }
}



module.exports = { createJWToken, verifyJWToken }
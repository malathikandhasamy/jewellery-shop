let Validator = require('validatorjs');
const bindRes = require('./bindRes');

let validateRequest = rule => (req, res, next) => {
    let { body } = req
    var validation = new Validator(body, rule)
    if (validation.passes()) {
        next()
    }
    else {
        let err = validation.errors.all();
        bindRes(true, "Request Failed", res, err)
    }
}


module.exports = validateRequest
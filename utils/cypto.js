var AES = require("crypto-js/aes");
var CryptoJS = require("crypto-js");

let encryption = (data) => {
    var encrypted = AES.encrypt(JSON.stringify(data), "n2r4u7x!A%D*G-Ka");
    return encrypted.toString();
}

let decryption = (req, res, next) => {
    var decrypted = AES.decrypt(req.body, "n2r4u7x!A%D*G-Ka");
    req.body = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8)||'{}')
    next()
}
module.exports = { decryption, encryption }
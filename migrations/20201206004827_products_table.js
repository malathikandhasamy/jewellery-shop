
exports.up = function (knex) {
    return knex.schema.createTable('products', function (table) {
        table.increments('product_code');
        table.string('product_name');
        table.integer('product_price');
        table.decimal('product_weight', 6, 2);
        table.timestamps(true, true);
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('products')
};

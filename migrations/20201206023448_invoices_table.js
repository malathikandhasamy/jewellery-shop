
exports.up = function(knex) {
    return knex.schema.createTable('invoices', function (table) {
        table.increments();
        table.integer('created_by');
        table.text('to_address');
        table.decimal('total',14,2)
        table.timestamps(true, true);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('invoices')
};

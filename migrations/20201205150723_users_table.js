
exports.up = function (knex) {
    return knex.schema.createTable('users', function (table) {
        table.increments();
        table.string('name');
        table.string('password');
        table.boolean('is_active');
        table.timestamps(true,true);

    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('users')
};


exports.up = function(knex) {
    return knex.schema.createTable('invoice_items', function (table) {
        table.increments();
        table.integer('invoice_id');
        table.string('product_name');
        table.integer('product_price');
        table.decimal('product_weight', 6, 2);
        table.integer('quantity');
        table.integer('gst');
        table.decimal('total',14,2)
        table.timestamps(true, true);
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('invoice_items')
};

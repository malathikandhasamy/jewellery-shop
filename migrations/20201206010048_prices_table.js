
exports.up = function (knex) {
    return knex.schema.createTable('prices', function (table) {
        table.increments();
        table.string('price_name');
        table.decimal('price', 14, 2);
        table.integer('gst');
        table.timestamps(true, true);
    })
};

exports.down = function (knex) {
    return knex.schema.dropTable('prices')
};
